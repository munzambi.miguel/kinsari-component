<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth')->middleware('prevent-back-history');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth')->middleware('prevent-back-history');
Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout')->middleware('auth');
Route::get('/login', 'App\Http\Controllers\Auth\LoginController@login')->name('login');

/*
Route::group(['prefix'=>'fichafuncionario','middleware'=>['prevent-back-history','auth']], function() {
    Route::get('/',function(){
       return redirect()->to('http://fichafuncionario.assembleia.apps');
    });
});
*/

/** ------------------------------------------------------------
 *  ROTAS DE GESTÃO DE UTILIZADORES : CRIAÇÃO, EDIÇÃO, REMOÇÃO
 *  ------------------------------------------------------------
 */
Route::group(['prefix' => 'user', 'middleware' => ['prevent-back-history', 'auth']], function () {

    Route::get('/', 'App\Http\Controllers\UserController@index')->name('listaUsers')->middleware('auth.admin');
    Route::get('/userProfile', 'App\Http\Controllers\UserController@perfilUtilizador')->name('perfilUtilizador');

    Route::get('/listagemPDF', 'App\Http\Controllers\UserController@listaPDF')->name('listaUsersPDF');

    Route::get('/show/{id}', 'App\Http\Controllers\Web\UserController@show')->name('showUser')->middleware('auth.admin');
    Route::get('/criar', 'App\Http\Controllers\Web\UserController@create')->name('formNovoUser')->middleware('auth.admin');
    Route::post('/novoRegisto', 'App\Http\Controllers\Web\UserController@store')->name('criaNovoUser')->middleware('auth.admin');
    Route::get('/edita/{user}', 'App\Http\Controllers\Web\UserController@edit')->name('formEditaUser');
    Route::get('/editaPass/{user}', 'App\Http\Controllers\Web\UserController@editPass')->name('formEditaPassUser');
    Route::put('/editaRegisto/{user}', 'App\Http\Controllers\Web\UserController@update')->name('editaUser');
    Route::put('/editaPassRegisto/{user}', 'App\Http\Controllers\Web\UserController@updatePass')->name('editaPassUser');

    Route::delete('/remove/{user}', 'App\Http\Controllers\Web\UserController@destroy')->name('removeUser');
    Route::get('/verLogs/{user}', 'App\Http\Controllers\Web\UserController@verLogs')->name('verLogsUser');
    Route::get('/reset/{user}', 'App\Http\Controllers\Web\UserController@resetPass')->name('resetPass');

    Route::get('/impersonate/{user}', 'App\Http\Controllers\Web\UserController@impersonateUser')->name('impersonate');
    Route::get('/leaveimpersonate', 'App\Http\Controllers\UserController@leaveimpersonateUser')->name('leaveimpersonate');

    Route::get('/permissoes', 'App\Http\Controllers\Comuns\PermissoesController@index')->name('permissoes')->middleware('auth.admin');
});

Route::group(['prefix' => 'Configuracoes', 'middleware' => ['prevent-back-history', 'auth']], function () {
    Route::get('/Configuracoes', 'App\Http\Controllers\Configuracoes\ConfiguracoesController@index')->name('Configuracoes');
});


Route::group(['prefix' => 'manager', 'middleware' => ['prevent-back-history', 'auth']], function () {
    Route::get('/', 'App\Http\Controllers\Departments\DashManagerController@index')->name('manager')->middleware('auth.isManager');
    /// vamos passar a routo das faltas marcadas pelo manager de forma propositada vejamos
});

Route::group(['prefix' => 'drh', 'middleware' => ['prevent-back-history', 'auth']], function () {
    Route::get('/', 'App\Http\Controllers\Departments\DashRHController@index')->name('rh')->middleware('auth.drh');
});

Route::group(['prefix' => 'funcionario', 'middleware' => ['prevent-back-history', 'auth']], function () {
    Route::get('/', 'App\Http\Controllers\Departments\DashFuncionarioController@index')->name('funcionario')->middleware('auth');
});

Route::group(['prefix' => 'sgan', 'middleware' => ['prevent-back-history', 'auth']], function () {
    Route::get('/', 'App\Http\Controllers\Departments\DashSGANController@index')->name('sgan')->middleware('auth.drh');
});



Route::post('dropzone/store', [\App\Http\Livewire\Forms\JustificacaoFaltas\JustificacaoForm::class, 'dropzoneStore'])->name('dropzone.store');

//Auth::routes();

Route::get('/testeAPI', 'App\Http\Controllers\UserController@testeAPI');
