<?php

namespace App\Models\Traits\Perfis;

use Adldap\Laravel\Facades\Adldap;
use App\Models\Comuns\OutInOffice;
use App\Models\External\AD\AD;


/**
 * Trait ApplicationUser
 * @package App\Models\Traits\Perfis
 *
 * Este contrato deve ser incluído em todas as aplicações (no modelo User).
 */
trait ApplicationUser
{

    /**
     * Verifica se o utilizador é Administrador da aplicação.
     * @return bool
     */
    protected function scopeIsAdmin()
    {
        // Se está a utilizar LDAP como meio de autenticação
        if (config('kinsari.utilizaldap')) {
            AD::init();
            if (AD::membroDe($this->name, config('kinsari.nomeGrupoAdmins')))
                return true;
            else
                return false;
        } else // Se não esta a utilizar LDAP , verifica na tabela Roles
        {
            return $this->hasRole('Administrador');
        }

    }

    /**
     * Verifica se o tuilizador é Super Admin
     * Esta lista é definida no ficheiro .env (SUPER_ADMINS = 'jose.ferreira@kinsari.com,miguel.araujo@kinsari.com')
     * Deve ser utilizado apenas por responsáveis da Kinsari pois dá acesso ao IMPERSONATE
     * @return bool
     */
    protected function scopeIsSuperAdmin()
    {
        $superAdmins = explode(',', config('kinsari.superAdmins'));
        if (in_array($this->email, $superAdmins))
            return true;
        else
            return false;
    }

    protected function scopeIsOutOfTheOffice()
    {
        OutInOffice::whereDate('inOffice', \Date::now()->format('Y-m-d'))->delete();
        // Se está a utilizar LDAP como meio de autenticação
        if (config('kinsari.utilizaldap') && $this->isManager()) {

            if (OutInOffice::where('managerEmail', $this->ldap->getEmail())->whereDate('outOffice', \Date::now()->format('Y-m-d'))->first())
                return true;
            else
                return false;

        } else // Se não esta a utilizar LDAP , verifica na tabela Roles
        {
            return $this->hasRole('Administrador');
        }

    }

    public function scopeIsInterinando()
    {

        OutInOffice::whereDate('inOffice', \Date::now()->format('Y-m-d'))->delete();
        //  && !$this->isManager()
        // Se está a utilizar LDAP como meio de autenticação
        if (config('kinsari.utilizaldap')) {

            if (OutInOffice::where('email', $this->ldap->getEmail())->whereDate('outOffice', \Date::now()->format('Y-m-d'))->first())
                return true;
            else
                return false;

        } else // Se não esta a utilizar LDAP , verifica na tabela Roles
        {
            return $this->hasRole('Administrador');
        }
    }
}
