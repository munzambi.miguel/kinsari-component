<?php


namespace App\Models\External\AD;


use Adldap\Laravel\Facades\Adldap;
use Exception;

class AD
{

    static private $provider;

    private function __construct()
    {
    }

    public static function init()
    {
        $providerConfig = config('ldap_auth.connection');

        self::$provider = Adldap::getProvider($providerConfig);
    }

    public static function membroDe($userName, $grupo)
    {
        try {
            $user = self::$provider->search()->whereEquals('cn', $userName)->firstOrFail();

            if (in_array($grupo, $user->getGroupNames()))

                return true;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function meuSubordinado($ldapInstance, $email = null)
    {

        self::init();
        try {
            foreach ($ldapInstance['directreports'] as $myDn) {
                $user = self::$provider->search()->findByDn($myDn);
                if ($email != null) {
                    $users[] = ['CN' => $user->getName(), 'Email' => $user->getEmail(), 'Departament' => $user->getDepartment()];
                } else {
                    $users[] = ['Email' => $user->getEmail()];
                }

            }
            return $users;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function meuSubordinadoObjects($ldapInstance)
    {

        self::init();
        try {
            foreach ($ldapInstance['directreports'] as $myDn) {
                $user = self::$provider->search()->findByDn($myDn);
                $users[] = ['CN' => $user->getName(), 'Email' => $user->getEmail(), 'Departament' => $user->getDepartment(), 'obj' => $user];
            }
            return $users;
        } catch (Exception $e) {
            return null;
        }
    }



    public static function subordinado($myDn)
    {
        self::init();
        try {
            $user = self::$provider->search()->findByDn($myDn);
            return $user->getName() ? $user->getName() : '<span class="text-warning font-italic">informação não encontrado</span>';
        } catch (\Exception $ed) {
            return '<span class="text-warning font-italic">informação não encontrado</span>';
        }
    }

    public static function getManager($myDn)
    {
        self::init();
        try {
            $user = self::$provider->search()->findByDn($myDn);
            return ['Name' => $user->getName(), 'Email' => $user->getEmail(), 'objectGuid' => $user];
        } catch (\Exception $ed) {
            return '<span class="text-warning font-italic">informação não encontrado</span>';
        }

    }

    public static function getUserCn($name)
    {
        self::init();
        try {
            $user = self::$provider->search()->findByDn(self::$provider->search()->users()->find($name));
            return $user->getDepartment() ? $user->getDepartment() : '<span class="text-warning">não foi encontrado</span>';
        } catch (\Exception $ed) {
            return get_class($ed);
        }
    }

    public static function all()
    {
        // resolveUsersUsing
        self::init();
        //CN=Users,DC=an,DC=local CN=Miguel Glory,DC=an,DC=local
        try {
            $user = self::$provider->search()->findByDn(self::$provider->search()->users()->find('alvaro'));;
            return $user;
        } catch (\Exception $ed) {
            return get_class($ed);
        }
    }

    public static function getUserAD($userName)
    {
        self::init();
        try {
            $user = self::$provider->search()->findByDn(self::$provider->search()->users()->find($userName));

            return [
                'name' => $user->getDisplayName(),
                'department' => collect($user['department'])->first(),
                'email' => collect($user['mail'])->first(),
                'cn' => collect($user['distinguishedname'])->first(),
            ];
        } catch (\Exception $d) {
            return ['name' => '', 'department' => '', 'email' => '', 'cn' => ''];
        }
    }

    public static function getUserADList($userName)
    {
        self::init();
        try {
            $user = self::$provider->search()->users()->find($userName);
            return $user;
        } catch (\Exception $d) {
            return ['name' => '', 'department' => ''];
        }
    }


    public static function getUserDc($dc)
    {
        self::init();
        try {
            $user = self::$provider->search()->findByDn($dc);
            return $user;
        } catch (\Exception $ed) {
            return null;
        }
    }


}
