<?php

namespace App\Models\Comuns;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $connection = 'ConnectionName';
    protected $table = 'SchemaName.Groups';

    protected $fillable = ['web', 'name', 'observation'];


    public static $routes = [
        'DashBoardFuncionario' => ['name' => 'Justificação de Faltas', 'web' => 'DashBoardFuncionario'],
        'DashBoardManager' => ['name' => 'Justificação Director', 'web' => 'DashBoardManager'],
        'DashBoardDrh' => ['name' => 'Justificação DRH', 'web' => 'DashBoardDrh'],
    ];


}
