<?php

namespace App\Models;

use Adldap\Laravel\Traits\HasLdapUser;
use App\Models\External\AD\AD;
use App\Models\Comuns\GroupMember;
use App\Models\Comuns\OutInOffice;
use App\Models\Traits\Perfis\ApplicationUser;
use App\Models\Traits\Perfis\AssembleiaUser;
use App\Models\Traits\Search;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Spatie\Permission\Traits\HasRoles;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasRoles;
    use HasLdapUser;
    use Search;
    use Impersonate;
    use ApplicationUser, AssembleiaUser;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $searchable = [
        'name',
        'email',
    ];

    public function canImpersonate()
    {
        return $this->isSuperAdmin();
    }

    public function updateUtilizador($validatedData)
    {
        $this->update($validatedData);
        $this->roles()->detach();
        $this->assignRole($validatedData['perfil']);

    }

    public function getAvatarUrlAttribute()
    {

        if ($this->avatar) {
            return asset('storage/avatars/' . $this->avatar);
        } else {
            return asset('dist/img/avatar5.png');
        }
    }

    public function getMyDirectPorts($email = null)
    {
        $intery = OutInOffice::where('email', $this->ldap->getEmail())->first();
        $interyManager = OutInOffice::where('managerEmail', $this->ldap->getEmail())->first();

        if ($intery) {
            return $email ? AD::meuSubordinado(AD::getUserDc($intery['managerCn'])) : AD::meuSubordinado(AD::getUserDc($intery['managerCn']), $email);
        } else if (!$interyManager) {
            return $email ? AD::meuSubordinado($this->ldap) : AD::meuSubordinado($this->ldap, $email);
        } else {
            return [];
        }


    }

    public function isManager()
    {
        return (collect($this->ldap['directreports'])->count() > 0);
    }

    public function getSubordinados()
    {
        /**
         * $intery = OutInOffice::where('email', $this->ldap->getEmail())->first();
         * $interyManager = OutInOffice::where('managerEmail', $this->ldap->getEmail())->first();
         *
         * if ($intery) {
         * return null;
         * } else if (!$interyManager) {
         * return AD::meuSubordinadoObjects($this->ldap);
         * } else
         * return null;
         */
        return AD::meuSubordinadoObjects($this->ldap);
    }

    public function isDrh(): bool
    {
        try {
            return $this->grouMember('DashBoardDrh');
        } catch (\Exception $de) {
            return false;
        }
    }

    public function isMemberDrAdmin()
    {
        try {
            return $this->grouMemberAdmin('DashBoardDrh');
        } catch (\Exception $de) {
            return false;
        }

    }


    public function grouMember($web)
    {

        $data = GroupMember::with('group')->where('email', \Auth::user()->ldap->getEmail())
            ->whereHas('group', function ($e) use ($web) {
                return $e->where('web', $web);
            })->first();

        return $data ? true : false;
    }

    public function grouMemberAdmin($web)
    {

        $data = GroupMember::with('group')->where('email', \Auth::user()->ldap->getEmail())
            ->where('isAdmin', true)
            ->whereHas('group', function ($e) use ($web) {
                return $e->where('web', $web);
            })->first();

        return $data ? true : false;
    }
}
