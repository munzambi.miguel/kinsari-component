<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÕES PRIMAVERA
    |--------------------------------------------------------------------------
    |
    */

    'ESTABELECIMENTO_AGENTES' => '098',

    'ESTABELECIMENTO_DEPUTADOS' => '099',

    /*
    |--------------------------------------------------------------------------
    | CONFIGURAÇÕES ACTIVE DIRECTORY
    |--------------------------------------------------------------------------
    |
     */
    'utilizaldap' => true,
    'nomeGrupoAdmins' => 'Aplicacoes.Admins',
    'superAdmins' => 'jose.ferreira',

    'KINSARI_APP_NAME' => env('APP_NAME')
];

