<div>
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('home')}}" class="brand-link">
            <span class="brand-text font-weight-light text-uppercase"> A.N De Angola  </span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{Auth::user()->getAvatarUrlAttribute()}}" style="width:45px;height:45px;"
                         class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="{{route('perfilUtilizador')}}" class="d-block">
                        <div style="overflow: hidden; text-overflow: ellipsis; width:100%;
                                 border:1px; white-space:nowrap; ">
                            {{Auth::user()->name}}
                        </div>
                    </a>
                    <a href="{{route('logout')}}"><span class="right badge badge-danger">Sair</span></a>
                    @impersonating
                    <a href="{{route('leaveimpersonate')}}"><span class="right badge badge-primary">Leave</span></a>
                    @endImpersonating
                </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    @if(Auth::user()->isAgenteAssembleia())
                        <li class="nav-header">AGENTE</li>
                    @endif
                    @if(Auth::user()->isDeputado())
                        <li class="nav-header">DEPUTADO</li>
                    @endif
                <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{route('home')}}"
                           class="nav-link {{(request()->is('home')) ? 'disabled active-sub' :''}}">
                            <i class="nav-icon fa fa-home "></i>
                            <p>Inicio</p>
                        </a>
                    </li>

                    <li class="nav-item {{ (request()->is('funcionario*')) ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ (request()->is('funcionario*')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Funcionário
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('funcionario')}}"
                                   class="nav-link {{(request()->is('funcionario')) ? 'disabled active-sub' :''}}">
                                    <i class="nav-icon far fa-circle text-info"></i>
                                    <p>Gerir Tarefas</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    @if(Auth::user()->isManager() || Auth::user()->isSuperAdmin() || Auth::user()->isAdmin() || Auth::user()->isInterinando())
                        <li class="nav-item {{ (request()->is('manager*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('manager*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-tasks"></i>
                                <p>
                                    Director / Manager
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('manager')}}"
                                       class="nav-link {{(request()->is('manager')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gerir Tarefas</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    @if(Auth::user()->isDrh() || Auth::user()->isSuperAdmin() || Auth::user()->isAdmin() )
                        <li class="nav-item {{ (request()->is('drh*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('drh*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-tasks"></i>
                                <p>
                                    Direção da RH
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('rh')}}"
                                       class="nav-link {{(request()->is('drh')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gerir Tarefas</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    @endif

                    @if(Auth::user()->isDrh() || Auth::user()->isSuperAdmin() || Auth::user()->isAdmin() )
                        <li class="nav-item {{ (request()->is('sgan*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('sgan*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-tasks"></i>
                                <p>
                                    Direção da SGAN
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('sgan')}}"
                                       class="nav-link {{(request()->is('sgan')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gerir Tarefas</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    @endif

                    @if(Auth::user()->isAdmin())
                        <li class="nav-header">ADMINISTRADOR</li>
                        <li class="nav-item {{ (request()->is('user*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('user*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>
                                    Utilizadores
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('listaUsers')}}"
                                       class="nav-link {{(request()->is('user')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gerir</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('permissoes')}}"
                                       class="nav-link {{(request()->is('user/permissoes')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon fas fa-users-cog text-info"></i>
                                        <p>Permissões</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item {{ (request()->is('Configuracoes*')) ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ (request()->is('Configuracoes*')) ? 'active' : '' }}">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    Configurações
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('Configuracoes')}}"
                                       class="nav-link {{(request()->is('Configuracoes/Configuracoes')) ? 'disabled active-sub' :''}}">
                                        <i class="nav-icon fas fa-tags text-info"></i>
                                        <p>Configurações</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif


                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

</div>
