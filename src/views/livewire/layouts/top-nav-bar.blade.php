<div>
    <nav class="main-header navbar navbar-expand  navbar-light " id="naveBar">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link"><i class="fa fa-home"></i> {!! $topInformation !!}  </a>
            </li>
        </ul>


        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            {{-- <li class="nav-item dropdown">
                 <a class="nav-link" data-toggle="dropdown" href="#">
                     <i class="far fa-comments"></i>
                     <span class="badge badge-success navbar-badge"
                           wire:poll.10000ms="updateNotificacao">{{ $funcionarioPublicados }}</span>
                 </a>
                 <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                     <br>
                     @forelse($evaluationStatus as $obj)

                         <a href="#" class="dropdown-item card">
                             <!-- Message Start -->
                             <div class="media">
                                 <div class="media-body">

                                     <h3 class="dropdown-item-title">
                                         <div style="overflow: hidden; text-overflow: ellipsis; width:200px;
                                         border:1px; white-space:nowrap; ">
                                             {{ $obj['nomeFuncionario'] }}
                                         </div>
                                     </h3>
                                     <br>
                                     <p class="text-sm text-secondary">
                                         Teve boa qualificação e consideração do Secretário da Assembleia
                                         nacional
                                     </p>
                                     <p class="text-sm text-success">
                                         {{ $obj['evaluation_year'] }}
                                     </p>

                                 </div>
                             </div>
                             <!-- Message End -->
                         </a>
                     @empty
                         <a href="#" class="dropdown-item">
                             <!-- Message Start -->
                             <div class="media">
                                 <div class="media-body">
                                     <h3 class="dropdown-item-title">
                                         {{ date('Y') . ' - Avaliação de desempenho' }}
                                         <span class="float-right text-sm text-warning">
                                             <i class="fas fa-star"></i>
                                         </span>
                                     </h3>
                                     <p class="text-sm">Não foi encontrado funcionario Publicado...</p>
                                     <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                 </div>
                             </div>
                             <!-- Message End -->
                         </a>
                     @endforelse
                     <div class="card-footer text-center bg-primary">
                         <a href="{{ $url }}">ver avaliações <i class="fas fa-arrow-circle-right"></i></a>
                     </div>

                 </div>

             </li>--}}

            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('home')}}" class="nav-link"><i class="fa fa-question"></i> Ajuda</a>
            </li>


        </ul>
    </nav>
</div>
