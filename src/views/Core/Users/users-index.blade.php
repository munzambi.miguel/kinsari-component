@extends('layouts.app')

@section('content')
    @livewire('comuns.sub-header',[
    'titleApplication'=>'Justificação de faltas',
    'titleArea'=>'Gestão de utilizadores',
    'departament'=>'Utilizadores',])

    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        {{-- COMPONENTE DE LISTAGEM E GESTÃO DE UTILIZADORES --}}
                        @livewire('listas.users.user-list')
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('third_party_scripts')
    <script>
        $(document).ready(function () {
            toastr.options = {
                "positionClass": "toast-bottom-right",
                "progressBar": true,
            }

            window.addEventListener('mostra-erro', event => {
                toastr.error(event.detail.message, 'Erro');
            });

            window.addEventListener('hide-form', event => {
                $('#formUser').modal('hide');

                toastr.success(event.detail.message, 'Sucesso');
            });

            window.addEventListener('show-form', event => {
                $('#formUser').modal('show');
            });

            window.addEventListener('show-delete-form', event => {
                $('#formConfirmation').modal('show');
            });
            window.addEventListener('hide-delete-form', event => {
                $('#formConfirmation').modal('hide');
                if (event.detail.error === true) {
                    toastr.error(event.detail.message, 'Erro');
                } else {
                    toastr.success(event.detail.message, 'Sucesso');
                }
            });
        });

    </script>
@endsection
