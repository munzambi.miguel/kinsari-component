@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .livewire-pagination {

            @apply inline-block w-auto ml-auto float-right;
        }

        ul.pagination {

            @apply flex border border-gray-200 rounded font-mono;
        }

        .page-link {

            @apply block bg-white text-blue-800 border-r border-gray-200 outline-none py-2 w-12 text-sm text-center;
        }

        .page-link:last-child {

            @apply border-0;
        }

        .page-link:hover {

            @apply bg-blue-700 text-white border-blue-700;
        }

        .page-item.active .page-link {

            @apply bg-blue-700 text-white border-blue-700;
        }

        .page-item.disabled .page-link {

            @apply bg-white text-gray-300 border-gray-200;
        }
    </style>
@endsection

@section('content')

    @livewire('comuns.sub-header',[
    'titleApplication'=>'',
    'titleArea'=>'Ambiente do Secretariado Geral',
    'departament'=>'Dashboard',])

    <div class="container-fluid ">
        @livewire('departments.rec-sgan.dashboard')
    </div>

@endsection
@section('third_party_scripts')
    <script>
        $(document).ready(function () {

            toastr.options = {
                "positionClass": "toast-bottom-right",
                "progressBar": true,
            }

            window.addEventListener('mostra-erro', event => {
                toastr.error(event.detail.message, 'Erro');
            });


            window.addEventListener('message-alert', evt => {
                toastr.warning(evt.detail.message, 'Seleciona a falta!');
            })

            window.addEventListener('show-form', event => {
                $('#formFuncionario').modal('show');
            });

            window.addEventListener('show-delete-form', event => {
                $('#formConfirmation').modal('show');
            });

            window.addEventListener('hide-delete-form', event => {
                $('#formConfirmation').modal('hide');
                if (event.detail.error === true) {
                    toastr.error(event.detail.message, 'Erro');
                } else {
                    toastr.success(event.detail.message, 'Sucesso');
                }
            });
        });

    </script>
@endsection
