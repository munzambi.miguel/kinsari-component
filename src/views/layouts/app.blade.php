<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>


    {{--<link rel="stylesheet" href="{{asset('plugins/boo')}}">--}}
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

    <link rel="shortcut icon" href="{{ asset('dist/img/AdminLTELogo.png') }}">
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/dropzone/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datetimepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">

    @livewireStyles
    <style>
        .active-sub {
            background-color: rgba(255, 255, 255, 0.06) !important;
            color: rgba(255, 255, 255, 0.78) !important;
        }

        @media (max-width: 1600px) {
            .layout {
                zoom: 84%;
            }

            .modal-backdrop {
                width: 200vw;
                height: 200vh;
            }
        }

        .datepicker {
            padding-left: 20px !important;
            padding-right: 20px !important;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
        }

        @media (min-width: 1600px) {
            .layout {
                zoom: 100%;
            }
        }
    </style>


    @yield('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout">
<div class="wrapper">

    @livewire('layouts.top-nav-bar')

    @livewire('layouts.side-bar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            @yield('content')
        </section>
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Versão</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2021 <a href="#">Kinsari Sistemas de Informação</a>.</strong>
    </footer>
</div>

<script src="{{ mix('js/app.js') }}"></script>
@include('sweetalert::alert')
<script src="{{ asset('plugins/select2/js/select2.full.js')}}"></script>
<script src="{{ asset('plugins/select2/js/i18n/pt.js')}}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('plugins/dropzone/min/dropzone.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datepicker.min.js') }}"></script>

@livewireScripts
@yield('third_party_scripts')

@stack('page_scripts')
@stack('scripts')


</html>
