<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ManagerAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Auth::check() and (\Auth::user()->isManager() || \Auth::user()->isSuperAdmin()) || \Auth::user()->isInterinando())
            return $next($request);
        else if (\Auth::check() and !(\Auth::user()->isManager() || \Auth::user()->isSuperAdmin()))
            return abort(401);

        return redirect('login');

    }
}
