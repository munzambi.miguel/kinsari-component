<?php

namespace App\Http\Livewire\Forms\Comuns;

use Livewire\Component;

class FaltasSelected extends Component
{
    public function render()
    {
        return view('livewire.forms.comuns.faltas-selected');
    }
}
