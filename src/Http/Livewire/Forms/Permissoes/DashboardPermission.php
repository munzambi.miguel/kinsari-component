<?php

namespace App\Http\Livewire\Forms\Permissoes;

use Livewire\Component;

class DashboardPermission extends Component
{
    public function render()
    {
        return view('livewire.forms.permissoes.dashboard-permission');
    }
}
