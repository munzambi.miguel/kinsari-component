<?php

namespace App\Http\Livewire\Departments\Employee;

use App\Models\Traits\DashboadFunctions;
use App\Models\Traits\SearchUtilitarios;
use App\Models\Traits\StatusProject;
use Livewire\Component;

class Dashboard extends Component
{
    use SearchUtilitarios;
    use StatusProject;
    use DashboadFunctions;

    public function render()
    {
        return view('livewire.departments.employee.dashboard');
    }
}
