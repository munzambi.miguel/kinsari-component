<?php

namespace App\Http\Livewire\Layouts;

use Livewire\Component;

class TopNavBar extends Component
{
    public $topInformation;

    public function mount()
    {
        try {

            $this->topInformation = \Auth::user()->ldap->getDepartment();
        } catch (\Exception $e) {
            $this->topInformation = '<span class="text-danger text-muted"> Departamento Anonimo </span>';
        }
    }

    public function render()
    {
        return view('livewire.layouts.top-nav-bar');
    }
}
