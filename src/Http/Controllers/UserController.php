<?php

namespace App\Http\Controllers;

use App\Models\External\MINFIN\MinfinAPI;
use App\Models\User;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $users = User::with('roles')->get();

        return view('Core.Users.users-index', compact('users'));
    }

    public function perfilUtilizador()
    {
        return view('Core.Users.user-profile');
    }

    public function listaPDF()
    {
        $utilizadores = User::all();
        $html =  View::make('Reports.user-report', compact('utilizadores'))->render();

        $pdf = PDF::loadHtml($html);
        return $pdf->download('RelatorioUtilizadores.pdf');
    }

    public function testeAPI()
    {
        MinfinAPI::listarAgentesUO();
    }

    public function leaveImpersonateUser()
    {
        Auth::user()->leaveImpersonation();

        return redirect()->route('home');
    }

}
