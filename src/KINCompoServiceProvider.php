<?php

/// "Kinsari\\Component\\KINCompoServiceProvider"
namespace Kinsari\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class KINCompoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


        $this->publishes([

            __DIR__ . '/Http' => app_path('Http'),
            __DIR__ . '/Models' => app_path('Models'),
            __DIR__ . '/routes' => base_path('routes'),
            __DIR__ . '/config' => base_path('config'),
            __DIR__ . '/views' => resource_path('views'),
            __DIR__ . '/database/migrations' => database_path('migrations'),

        ], 'kinsari/components');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
