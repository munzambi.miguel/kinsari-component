<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunsAppsKinsariOutInOfficeTable extends Migration
{
    /**
     * Run the migrations.
     * @ConnectionName is example connection database application
     * @SchemaName is example SchemaName database application
     *
     * if installed in your application change name.
     * because is very important to configure proprieties.
     *
     * Author: Munzambi Ntemo Miguel
     * @return void
     */
    public function up()
    {
        Schema::connection('ConnectionName')->create('SchemaName.OutInOffice', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('respName');
            $table->text('respEmail');
            $table->string('managerName');
            $table->text('managerEmail');
            $table->string('name');
            $table->text('email');
            $table->dateTime('outOffice');
            $table->dateTime('inOffice');
            $table->text('observation')->nullable();
            $table->dateTime('dateInOffice');
            $table->boolean('status')->nullable();
            $table->text('managerCn');
            $table->text('personCn');
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ConnectionName')->drop('SchemaName.OutInOffice', function (Blueprint $table) {


        });
    }
}
