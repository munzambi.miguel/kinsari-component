<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunsAppsKinsariGroupsTable extends Migration
{
    /**
     * Run the migrations.
     * @ConnectionName is example connection database application
     * @SchemaName is example SchemaName database application
     *
     * if installed in your application change name.
     * because is very important to configure proprieties.
     *
     * Author: Munzambi Ntemo Miguel
     * @return void
     */
    public function up()
    {
        Schema::connection('ConnectionName')->create('SchemaName.Groups', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('web');
            $table->text('name');
            $table->text('observation')->nullable();
            $table->timestamps();
            $table->unique('web');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ConnectionName')->drop('SchemaName.Groups', function (Blueprint $table) {


        });
    }
}
