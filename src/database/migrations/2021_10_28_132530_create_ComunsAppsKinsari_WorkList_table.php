<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunsAppsKinsariWorkListTable extends Migration
{
    /**
     * Run the migrations.
     * @ConnectionName is example connection database application
     * @SchemaName is example SchemaName database application
     *
     * if installed in your application change name.
     * because is very important to configure proprieties.
     *
     * Author: Munzambi Ntemo Miguel
     * @return void
     */
    public function up()
    {
        Schema::connection('ConnectionName')->create('SchemaName.WorkList', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('justificacaofalta_id')->nullable();
            $table->boolean('isManager')->nullable();
            $table->boolean('isDrh')->nullable();
            $table->text('nomeFuncionario')->nullable();
            $table->integer('idFuncionario')->nullable();
            $table->text('nomeManager')->nullable();
            $table->integer('idManager')->nullable();
            $table->boolean('isNotManager')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ConnectionName')->drop('SchemaName.WorkList', function (Blueprint $table) {










        });
    }
}
