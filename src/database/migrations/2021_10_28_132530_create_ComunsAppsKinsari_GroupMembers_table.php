<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunsAppsKinsariGroupMembersTable extends Migration
{
    /**
     * Run the migrations.
     * @ConnectionName is example connection database application
     * @SchemaName is example SchemaName database application
     *
     * if installed in your application change name.
     * because is very important to configure proprieties.
     *
     * Author: Munzambi Ntemo Miguel
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ConnectionName')->create('SchemaName.GroupMembers', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('auth');
            $table->string('authName');
            $table->string('groupId');
            $table->text('department')->nullable();
            $table->text('email');
            $table->string('name');
            $table->boolean('isAdmin')->default(false);
            $table->text('observation')->nullable();
            $table->text('cn');
            $table->timestamps();
            $table->unique(['name', 'groupId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ConnectionName')->drop('SchemaName.GroupMembers', function (Blueprint $table) {


        });
    }
}
